package com.example.narolki;

import android.app.Service;
import android.content.Intent;
import android.location.Location;

import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationListener;

import java.text.DecimalFormat;
import java.util.concurrent.TimeUnit;

public class LocationService extends Service implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener,
        LocationListener {
    private static  final long INTERVAL= 1000*2;
    private static  final long FASTEST_INTERVAL= 1000*2;
    static double distance=0;
    LocationRequest mLocationnRequest;
    GoogleApiClient mGoogleApiClient;
    Location mCurrentLocation, lStart, lEnd;
    TextView km;
    private final IBinder mBinder= new LocalBinder();


    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        createLocationRequest();
        mGoogleApiClient=new GoogleApiClient.Builder(this).addApi(LocationServices.API).addConnectionCallbacks(this).addOnConnectionFailedListener(this).build();
        mGoogleApiClient.connect();
        return mBinder;

    }

    private void createLocationRequest() {
        mLocationnRequest= new LocationRequest();
        mLocationnRequest.setInterval(INTERVAL);
        mLocationnRequest.setFastestInterval(FASTEST_INTERVAL);
        mLocationnRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);


    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        try {
            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationnRequest, (com.google.android.gms.location.LocationListener) this);
        }catch (SecurityException e)
        {



        }
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onLocationChanged(Location location) {
    MainActivity.pDialog.dismiss();
    mCurrentLocation= location;

    if (lStart==null)
    {
        lStart = lEnd=mCurrentLocation;
    }
    else
        lEnd=mCurrentLocation;
    updateUI();
    if (location==null)
    {
        MainActivity.km.setText("m/s");

    }
    else {
        float nCspeed =((location.getSpeed()*3600)/1000);
        MainActivity.km.setText(nCspeed+" km/h");
    }
    }


    private void updateUI() {

        if (MainActivity.p ==0){
            distance=distance+(lStart.distanceTo(lEnd));
            MainActivity.etime= System.currentTimeMillis();
             long diff=MainActivity.etime - MainActivity.stime;
             diff= TimeUnit.MILLISECONDS.toSeconds(diff);

             MainActivity.distance.setText(new DecimalFormat("#.###").format(distance)+" m");

             lStart=lEnd;



        }
    }


    public class LocalBinder extends Binder {
        public LocationService getService(){
        return LocationService.this;


        }

    }

    @Override
    public boolean onUnbind(Intent intent) {
        stopLocationUpdates();
        if(mGoogleApiClient.isConnected())
            mGoogleApiClient.disconnect();

        lStart=lEnd=null;
        distance=0;

        return super.onUnbind(intent);

    }

    private void stopLocationUpdates() {
        LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, (com.google.android.gms.location.LocationListener) this);
        distance=0;



    }
}


