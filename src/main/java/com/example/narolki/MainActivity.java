package com.example.narolki;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.ComponentName;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.pm.PackageManager;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.os.IBinder;
import android.os.SystemClock;
import android.provider.Settings;
import android.view.View;
import android.widget.Button;
import android.widget.Chronometer;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    static boolean status;
    LocationManager locationManager;
    static TextView distance,km;
    private Button stst,pause,stop;
    static long stime, etime;
    static ProgressDialog pDialog;
    static int p=0;
    private Chronometer chrono;
    LocationService mServ;
    private long timepause;
    private ServiceConnection ser= new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            LocationService.LocalBinder binder= (LocationService.LocalBinder) service;
            mServ =  binder.getService();
            status=false;

        }

        @Override
        public void onServiceDisconnected(ComponentName name) {

        }
    };
    @Override
    protected void onDestroy() {
        if(status==true)
        unbindService();
        super.onDestroy();
    }
    private void unbindService(){
        if(status ==false)
            return;
        Intent i =new Intent(getApplicationContext(), LocationService.class);
        unbindService(ser);
        status=false;


    }

    @Override
    public void onBackPressed() {
       if (status==false)
           super.onBackPressed();
       else
           moveTaskToBack(true);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case 1000:
                {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED)
                    Toast.makeText(this, "Granted", Toast.LENGTH_SHORT).show();
                else
                    Toast.makeText(this, "Denied", Toast.LENGTH_SHORT).show();
                }
                return;
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED ||
                ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED ||
                ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED)

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                requestPermissions(new String[]{
                        Manifest.permission.WRITE_EXTERNAL_STORAGE,
                        Manifest.permission.ACCESS_FINE_LOCATION,
                        Manifest.permission.ACCESS_COARSE_LOCATION



                },1000);
            }
        distance=(TextView) findViewById(R.id.distance);
        stst=(Button) findViewById(R.id.stst);
        pause=(Button) findViewById(R.id.pause);
        km=(TextView)findViewById(R.id.km);
        stop=(Button) findViewById(R.id.stop);
        chrono=(Chronometer) findViewById(R.id.chronometer);
        stst.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkGPS();
                locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
                if (!locationManager.isProviderEnabled(locationManager.GPS_PROVIDER))
                    return;
                if (status == false)
                    bindService();
                pDialog = new ProgressDialog(MainActivity.this);
                pDialog.setIndeterminate(true);
                pDialog.setCancelable(false);
                pDialog.setMessage("Pobieram lokacje..");
                pDialog.show();

                stst.setVisibility(View.GONE);
                pause.setVisibility(View.VISIBLE);
                pause.setText("PAUSE");
                stop.setVisibility(View.VISIBLE);
                if (timepause !=0 ){
                    chrono.setBase(chrono.getBase()+SystemClock.elapsedRealtime()-timepause);
                }
                else {
                    chrono.setBase(SystemClock.elapsedRealtime());
                }
                chrono.start();
            }
        });

        pause.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (pause.getText().toString().equalsIgnoreCase("PAUSE")) {
                    timepause = SystemClock.elapsedRealtime();
                    chrono.stop();
                    pause.setText("RESUME");
                    p = 1;
                } else if(pause.getText().toString().equalsIgnoreCase("RESUME"))
                {checkGPS();

                chrono.setBase(chrono.getBase()+SystemClock.elapsedRealtime()-timepause);
                chrono.start();
                    locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
                    if (!locationManager.isProviderEnabled(locationManager.GPS_PROVIDER))
                        return;
                    pause.setText("PAUSE");
                    p=0;
                }

            }
        });
        stop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(status==true)
                    unbindService();
                stst.setVisibility(View.VISIBLE);
                pause.setText("PAUSE");
                pause.setVisibility(View.GONE);
                stop.setVisibility(View.GONE);
                chrono.stop();
                chrono.setBase(SystemClock.elapsedRealtime());
                timepause=0;
            }
        });
}

    private void bindService() {
        locationManager=(LocationManager) getSystemService(LOCATION_SERVICE);
        if(!locationManager.isProviderEnabled(locationManager.GPS_PROVIDER))
            gpsdisable();
            

    }

    private void gpsdisable() {

        AlertDialog.Builder adb =new AlertDialog.Builder(this);
        adb.setMessage("Uruchom GPS by aplikacja działała").setCancelable(false).setPositiveButton("Uruchom GPS", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Intent in =new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                startActivity (in);


            }
        });

        adb.setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        AlertDialog add = adb.create();
        add.show();
    }

    private void checkGPS() {
        if (status==true)
            return;
        Intent i =new Intent(getApplicationContext(),LocationService.class);
        bindService(i,ser,BIND_AUTO_CREATE);
        status =true;
        stime= System.currentTimeMillis();
    }

}
